defmodule Adyen.PaymentNotification do

  def controller do
    quote do
      if Application.get_env(:adyen, :notifications, nil) != nil,
        do: plug BasicAuth, use_config: {:adyen, :notifications}
      plug Adyen.PaymentNotification.EnvValidator
    end
  end

  def is_authorisation?(notification) do
    notification["eventCode"] == "AUTHORISATION"
  end

  def is_successful?(notification) do
    notification["success"] == "true"
  end

  def send_accepted_response(conn) do
    conn
    |> Plug.Conn.put_resp_content_type("text/plain")
    |> Plug.Conn.send_resp(200, "200 [accepted]")
  end

  @doc """
  When used, dispatch to the appropriate controller/view/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end