defmodule Adyen.PaymentNotification.EnvValidator do

  def init(options), do: options

  def call(conn, _opts) do
    if Application.get_env(:adyen, :live, false) |> to_string != conn.params["live"],
      do: conn
          |> Plug.Conn.put_resp_content_type("text/plain")
          |> Plug.Conn.send_resp(403, "403 Forbidden"),
      else: conn
  end
end