defmodule Adyen.Codes do

  # AuthenticationFinished – The payment has been successfully authenticated with 3D Secure 2. Returned for 3D Secure 2 authentication-only transactions.
  defmacro payment_authenticated_finished, do: quote(do: "AuthenticationFinished")
  # Authorised – The payment was successfully authorised. This state serves as an indicator to proceed with the delivery of goods and services. This is a final state.
  defmacro payment_authorised, do: quote(do: "Authorised")
  # Cancelled – Indicates the payment has been cancelled (either by the shopper or the merchant) before processing was completed. This is a final state.
  defmacro payment_cancelled, do: quote(do: "Cancelled")
  # ChallengeShopper – The issuer requires further shopper interaction before the payment can be authenticated. Returned for 3D Secure 2 transactions.
  defmacro payment_challenge_shopper, do: quote(do: "ChallengeShopper")
  # Error – There was an error when the payment was being processed. The reason is given in the refusalReason field. This is a final state.
  defmacro payment_error, do: quote(do: "Error")
  # IdentifyShopper – The issuer requires the shopper's device fingerprint before the payment can be authenticated. Returned for 3D Secure 2 transactions.
  defmacro payment_identify_shopper, do: quote(do: "IdentifyShopper")
  # Pending – Indicates that it is not possible to obtain the final status of the payment. This can happen if the systems providing final status information for the payment are unavailable, or if the shopper needs to take further action to complete the payment. For more information, refer to Result codes.
  defmacro payment_pending, do: quote(do: "Pending")
  # PresentToShopper – Indicates that the response contains additional information that you need to present to a shopper, so that they can use it to complete a payment.
  defmacro payment_present_to_shopper, do: quote(do: "PresentToShopper")
  # Received – Indicates the payment has successfully been received by Adyen, and will be processed. This is the initial state for all payments.
  defmacro payment_received, do: quote(do: "Received")
  # RedirectShopper – Indicates the shopper should be redirected to an external web page or app to complete the authorisation.
  defmacro payment_redirect_shopper, do: quote(do: "RedirectShopper")
  # Refused – Indicates the payment was refused. The reason is given in the refusalReason field. This is a final state.
  defmacro payment_refused, do: quote(do: "Refused")
end

