defmodule Adyen.Client do

  require Logger

  def default_headers do
    [
      {"X-API-Key", Application.get_env(:adyen, :api_key, "")},
      {"Content-Type", "application/json"}
    ]
  end

  def default_options do
    [
      {:timeout, 60_000},
      {:recv_timeout, 60_000}
    ]
  end

  def debug_mode? do
    Application.get_env(:adyen, :debug, false)
  end

  def full_url(uri) do
    Application.get_env(:adyen, :base_url, "")<>"/"<>Application.get_env(:adyen, :api_version, "")<>uri
  end

  def get(uri) do
    with full_url <- full_url(uri) do
      if debug_mode?(),
        do: Logger.debug("Adyen - GET #{full_url}")
      res = HTTPoison.get(full_url, default_headers(), default_options())
      if debug_mode?(),
        do: Logger.debug("Adyen Response - GET #{full_url}\n#{inspect res}")
      res |> response()
    end
  end

  def post(uri, payload \\ %{}) do
    with full_url <- full_url(uri) do
      if debug_mode?(),
        do: Logger.debug("Adyen Request - POST #{full_url}\nPayload: #{inspect payload}")
      res = HTTPoison.post(full_url(uri), Poison.encode!(payload), default_headers(), default_options())
      if debug_mode?(),
        do: Logger.debug("Adyen Response - POST #{full_url}\n#{inspect res}")
      res |> response()
    end
  end

  def response({:ok, %HTTPoison.Response{body: "", status_code: code}}), do: {code, ""}

  def response({:ok, %HTTPoison.Response{body: raw, status_code: code}}), do: {code, Poison.decode!(raw)}

  def response({:error, %HTTPoison.Error{reason: :timeout}}), do: {:error, "Timeout"}

  def response({:error, %HTTPoison.Error{reason: ""}}), do: {:error, ""}

  def response({:error, %HTTPoison.Error{reason: reason}}), do: {:error, Poison.decode!(reason)}

end