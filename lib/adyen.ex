defmodule Adyen do

  def create_payment(payload) do
    Adyen.Client.post(
      "/payments",
      payload
      |> Map.merge(%{
        "merchantAccount" => Application.get_env(:adyen, :merchant_account, ""),
      })
    )
  end

  def create_payment_details(payload) do
    Adyen.Client.post(
      "/payments/details",
      payload
    )
  end

  def return_url(params) when is_nil(params) or params == %{}, do: Application.get_env(:adyen, :return_url, "")
  def return_url(params), do: Application.get_env(:adyen, :return_url, "") <> "?" <> URI.encode_query(params)

  def origin_url(), do: Application.get_env(:adyen, :origin, "")

end