
# Adyen

## Installation

[Adyen]can be installed
by adding `adyen` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:adyen, "~> 0.1.0"}
  ]
end
```

## Configuration

```elixir
config config :adyen,
  base_url: "https://checkout-test.adyen.com",
  api_version: "v53",
  api_key: "AQEohmfuXNWTK0Qc+iSEh3Y6qNSXSZJ5CIBe944x0HYjCqzirS5wZO+o7BDBXVsNv",
  debug: true,
  live: false,
  merchant_account: "MyMerchantECOM"
  notifications: [
    username: "test",
    password: "test"
  ],
  return_url: "https://my-app.com/checkout"
```

## Usage

### Create Payment

```elixir
payload = %{
                    "amount": {
                      "currency": "USD",
                      "value": 1000
                    },
                    "reference": "Your order number",
                    "paymentMethod": {
                      "type": "scheme",
                      "number": "4111111111111111",
                      "expiryMonth": "03",
                      "expiryYear": "2030",
                      "holderName": "John Smith",
                      "cvc": "737"
                    },
                    "returnUrl": Adyen.return_url(%{order_number: "Your order number"})
                  }

Adyen.create_payment(payload)
```

Example response:
```elixir
{
  200,
  %{
    "additionalData" => %{
      "expiryDate" => "3/2030",
      "recurringProcessingModel" => "CardOnFile",
      "recurring.recurringDetailReference" => "8415843843752276",
      "recurring.shopperReference" => "Shopper ID",
      "paymentMethod" => "mc",
      "cardHolderName" => "Test Guy",
      "cardSummary" => "0004"
    },
    "pspReference" => "882584387924366F",
    "resultCode" => "Authorised",
    "amount" => %{
      "currency" => "USD",
      "value" => 1000
    },
    "merchantReference" => "Your order number"
  }
} 
```

### Define a payment notifications endpoint

```elixir
defmodule MyApp.PaymentController do

  # Handle Adyen Notifications Basic Auth
  use Adyen.PaymentNotification, :controller

	# Payload example
	#	%{"live" => "false", "notificationItems" => [%{"NotificationRequestItem" => %{"additionalData" => %{"authCode" => "080353", "cardHolderName" => "Test Guy", "cardSummary" => "0000", "expiryDate" => "03/2030", "paymentMethod" => "visa", "recurring.recurringDetailReference" => "8315844023487379", "recurring.shopperReference" => "09df5647-bd75-4c94-85d2-41bfb7121690", "recurringProcessingModel" => "CardOnFile"}, "amount" => %{"currency" => "USD", "value" => 10000000}, "eventCode" => "AUTHORISATION", "eventDate" => "2020-03-17T01:24:39+01:00", "merchantAccountCode" => "TurboPlayTestECOM", "merchantReference" => "1584404671", "operations" => ["CANCEL", "CAPTURE", "REFUND"], "paymentMethod" => "visa", "pspReference" => "853584404679513D", "reason" => "080353:0000:03/2030", "success" => "true"}}]}

  def notification(conn, payload) do
    Enum.each(payload["notificationItems"], fn %{"NotificationRequestItem" => notification} ->
      if notification |> Adyen.PaymentNotification.is_authorisation?(),
         do: if notification |> Adyen.PaymentNotification.is_successful?(),
              do: IO.puts("Success."),
              else: IO.puts("Declined.")
    end)
    Adyen.PaymentNotification.send_accepted_response(conn)
  end
end
```