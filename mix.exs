defmodule Adyen.MixProject do
  use Mix.Project

  def project do
    [
      app: :adyen,
      version: "0.1.3",
      elixir: "~> 1.3",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      description: description(),
      package: package(),
      name: "Adyen",
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ],
      docs: [
        extras: ["README.md"],
        main: "readme"
      ]
    ]
  end

  defp description do
    """
    Adyen SDK
    """
  end

  defp package do
    [
      name: :adyen,
      maintainers: ["The Jibe Multimedia Inc."],
      licenses: ["MIT"],
      links: %{
        "Bitbucket" => "https://bitbucket.org/thejibe/elixir-adyen-sdk"
      }
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:poison, "~> 3.1"},
      {:httpoison, "~> 1.0"},
      {:excoveralls, "~> 0.8", only: [:test]},
      {:inet_cidr, "~> 1.0.0"},
      {:basic_auth, "~> 2.2.2"}
    ]
  end
end
